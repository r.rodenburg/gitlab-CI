# hadleyverse contains r-base with devtools
# we need devtools to install our R package from gitlab
FROM rocker/hadleyverse
MAINTAINER Ivo Lagarde <i.lagarde@anchormen.nl> 

# Install our own R package
#COPY install-packages.R install-packages.R
#COPY unittests.R unittests.R
#RUN Rscript install-packages.R
#RUN rm install-packages.R
COPY ./our-r-package ./our-r-package
COPY ./unittests.R ./unittests.R
COPY ./install-packages.R ./install-packages.R
RUN Rscript install-packages.R
RUN rm ./install-packages.R
RUN rm -R ./our-r-package
